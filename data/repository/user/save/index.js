import connect from "../../../db"
import User from "../../../models/user"

export const save = async (data) => {
    const con = await connect()

    const user = new User(data)
    await user.save()

    await con.disconnect()
}