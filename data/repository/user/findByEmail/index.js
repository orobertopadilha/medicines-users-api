import connect from "../../../db"
import User from "../../../models/user"

export const findByEmail = async (email) => {
    const con = await connect()
    const user = await User.findOne({ email })
    await con.disconnect()
    
    return user
}